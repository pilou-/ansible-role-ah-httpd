---
- name: Install httpd and related packages
  package:
    name: "{{ httpd_package }}"
    state: present

- name: "Retrieve httpd version #1"
  shell: |
    set -o pipefail
    {{ httpd_binary }} -v | grep 'Server version:' | cut -d / -f 2 | cut -d ' ' -f 1
  args:
    executable: /bin/bash
  changed_when: False
  register: httpd_version_raw
  check_mode: no

- name: "Retrieve httpd version #2"
  set_fact:
    httpd_version: "{{ httpd_version_raw.stdout }}"

- name: "Retrieve httpd version #3"
  debug:
    var: httpd_version

- name: Make sure httpd is running
  service:
    name: "{{ httpd_service }}"
    state: started
    enabled: yes

- name: Enable common httpd modules
  apache2_module:
    name: "{{ item }}"
    state: present
  with_items:
    - status
    - filter
    - rewrite
    - headers
  when: ansible_os_family == "Debian"

# it is more performant and required for HTTP2 since 2.4.27
- name: Fail if HTTP2 is enabled with old MPM
  fail:
    msg: "HTTP2 cannot work with the prefork MPM since Apache 2.4.27"
  when: disable_modern_mpm and not disable_http2 and httpd_version is version_compare('2.4.27', '>=')
#
- name: Enable Event MPM (RedHat)
  lineinfile:
    path: /etc/httpd/conf.modules.d/00-mpm.conf
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    backrefs: True
  with_items:
    - regexp: "^#(LoadModule mpm_event_module .*)$"
      line: "\\1"
    - regexp: "^(LoadModule mpm_(?:prefork|worker)_module .*)$"
      line: "#\\1"
  when: not disable_modern_mpm and ansible_os_family == "RedHat"
  notify: restart httpd
#
- name: Enable Event MPM (Debian)
  block:
    - name: Disable other MPMs
      apache2_module:
        name: "{{ item }}"
        state: absent
        ignore_configcheck: yes
      with_items:
        - mpm_prefork
        - mpm_worker
      notify: restart httpd
    - name: Enable event MPM
      apache2_module:
        name: mpm_event
        state: present
        ignore_configcheck: yes
      notify: restart httpd
  when: not disable_modern_mpm and ansible_os_family == "Debian"

# conflicts with our own because of precedence
- name: Remove package-provided security configuration
  file:
    path: "{{ httpd_global_confdir }}/security.conf"
    state: absent
  notify: reload httpd
  when: ansible_os_family == "Debian"

- name: Install common httpd configuration
  template:
    src: "common_global.conf"
    dest: "{{ httpd_global_confdir }}/common.conf"
    owner: root
    group: "{{ httpd_usergroup }}"
    mode: 0644
  notify: restart httpd

- name: Open firewall (EL6)
  command: "lokkit -s {{ item }}"
  with_items:
    - http
    - https
  when: manage_firewall and ansible_distribution_major_version|int <= 6 and (ansible_distribution == 'CentOS' or ansible_distribution == 'RedHat')

- name: Open firewall (firewalld)
  firewalld:
    service: "{{ item }}"
    permanent: true
    state: enabled
    immediate: yes
  with_items:
    - http
    - https
  when: manage_firewall and ((ansible_distribution == 'Fedora') or (ansible_os_family == "RedHat" and ansible_distribution_major_version|int >= 7) or (ansible_os_family == "Debian"))

- name: Set _httpd_common
  set_fact:
    _httpd_common: True

