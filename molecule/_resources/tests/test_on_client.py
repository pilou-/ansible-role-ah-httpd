import os

import testinfra.utils.ansible_runner

testinfra_ansible_runner = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE'])
testinfra_hosts = testinfra_ansible_runner.get_hosts('ansible-test-client')

web_facts = testinfra_ansible_runner.run_module('ansible-test-web', 'setup', None)['ansible_facts']
web_ip = web_facts['ansible_default_ipv4']['address']


# further tests are run from the client node to test a real network connection
# it would then fail if the firewall is not right
# it is also needed because the EL7 curl version does not support the --resolve option

def test_web_default_vhost(host):
    result_file = '/tmp/test_web_default_vhost_mainpage'
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -o {result_file} http://ansible-test-web.example.com/")
    assert host.file(result_file).contains("Testing 123..")

def test_web_test1_vhost_mainpage(host):
    result_file = '/tmp/test_web_test1_vhost_mainpage'
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -o {result_file} http://test1.example.com/")
    assert host.file(result_file).content_string == "test1.example.com/mainpage"

# redirection to the main page
def test_web_test1_vhost_redir1(host):
    result_file = '/tmp/test_web_test1_vhost_redir1'
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -L -o {result_file} http://test1.example.com/redir1/")
    assert host.file(result_file).content_string == "test1.example.com/mainpage"

# redirection at vhost root using the same page
def test_web_test1_vhost_redir2(host):
    result_file = '/tmp/test_web_test1_vhost_redir2'
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -L -o {result_file} http://test1.example.com/redir2/example1.html")
    assert host.file(result_file).content_string == "test1.example.com/example1"

def test_web_test1_vhost_headers(host):
    headers_file = '/tmp/test_web_test1_vhost_headers'
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -D {headers_file} http://test1.example.com/")
    assert host.file(headers_file).contains("X-Content-Type-Options: nosniff")
    assert host.file(headers_file).contains("X-Frame-Options: SAMEORIGIN")
    assert host.file(headers_file).contains("Referrer-Policy: same-origin")
    assert host.file(headers_file).contains("Content-Security-Policy: fake-policy")
    assert host.file(headers_file).contains("Feature-Policy:")

# redirection of the whole vhost
def test_web_test2_vhost_mainpage(host):
    result_file = '/tmp/test_web_test2_vhost_mainpage'
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -L -o {result_file} http://test2.example.com/")
    assert host.file(result_file).content_string == "test1.example.com/mainpage"

def test_web_test3_vhost_mainpage(host):
    headers_file = '/tmp/test_web_test3_vhost_headers'
    result_file = '/tmp/test_web_test3_vhost_mainpage'
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -D {headers_file} http://test3.example.com/")
    assert host.file(headers_file).contains("HTTP/1.1 401 Unauthorized")
    host.run_expect([0], f"curl --resolve '*:80:{web_ip}' -u testuser:testpwd -o {result_file} http://test3.example.com/")
    assert host.file(result_file).content_string == "test3.example.com/mainpage"

